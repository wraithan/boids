# boids

```shell
cargo run --release
```

See pretty flocking, I'll add options/settings file stuff with time.

## contributing

I'm not currently interested in contributions, this is a learning project. It is open so others can fork or learn from how I've done things. If you are a friend feel free to submit feedback, ideas, and code review.

## license

MIT/Apache 2.0 take your pick, I appreciate attribution but not required.