#version 400 core

in vec2 v_uv;

uniform Uniforms {
    vec4 color;
    mat4 proj_view;
    vec2 boid_pos;
    vec2 boid_scale;
} u;

out vec4 Target0;

void main() {
    vec2 uv = v_uv - vec2(0.5);
    float m = 1.0 - smoothstep(-0.1, 0.1, dot(uv, uv) * 4.0);
    Target0 = u.color * m * 5.0;
}
