#version 400 core

in vec2 in_pos;
in vec2 in_uv;

uniform Uniforms {
    vec4 color;
    mat4 proj_view;
    vec2 boid_pos;
    vec2 boid_scale;
} u;

out vec2 v_uv;

void main() {
    v_uv = in_uv;

    gl_Position = u.proj_view * vec4(u.boid_pos + (in_pos * u.boid_scale), 0.0, 1.0);
}