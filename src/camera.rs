use nalgebra as na;
use nalgebra_glm as glm;
use specs::{Read, System, World, WriteExpect};

pub struct Camera {
    // Calculation inputs
    projection: na::Perspective3<f32>,
    position: glm::Vec3,
    yaw: f32,
    pitch: f32,

    // For reset
    origin: glm::Vec3,
    original_at: glm::Vec3,

    // Precomputed values/caching
    proj: glm::Mat4,
    view: glm::Mat4,
    pub proj_view: glm::Mat4,
    pub inverse_proj_view: glm::Mat4,
}

impl Camera {
    fn new<PV: Into<glm::Vec3>, AV: Into<glm::Vec3>>(position: PV, at: AV, aspect: f32) -> Self {
        let mut camera = Self {
            position: glm::zero(),
            yaw: 0.0,
            pitch: 0.0,

            origin: position.into(),
            original_at: at.into(),

            projection: make_perspective(aspect),
            proj: glm::zero(),
            view: glm::zero(),
            proj_view: glm::zero(),
            inverse_proj_view: glm::zero(),
        };
        camera.reset();
        camera
    }

    fn look_at(&mut self, position: glm::Vec3, at: glm::Vec3) {
        let dist = (position - at).norm();

        let pitch = ((at.y - position.y) / dist).acos();
        let yaw = (at.z - position.z).atan2(at.x - position.x);

        self.position = position;
        self.yaw = yaw;
        self.pitch = pitch;
        self.update_proj_view();
    }

    fn update_proj_view(&mut self) {
        self.view = self.view_transform(); //.to_homogeneous();
        self.proj = *self.projection.as_matrix();
        self.proj_view = self.proj * self.view;
        self.inverse_proj_view = self
            .proj_view
            .try_inverse()
            .expect("Couldn't get inverse of camera transform");
    }

    fn view_transform(&self) -> glm::Mat4 {
        glm::look_at_rh(&self.position, &self.at(), &glm::Vec3::y())
    }

    pub fn at(&self) -> glm::Vec3 {
        let ax = self.position.x + self.yaw.cos() * self.pitch.sin();
        let ay = self.position.y + self.pitch.cos();
        let az = self.position.z + self.yaw.sin() * self.pitch.sin();

        glm::vec3(ax, ay, az)
    }

    fn reset(&mut self) {
        self.look_at(self.origin, self.original_at);
    }

    fn resize(&mut self, aspect: f32) {
        self.projection = make_perspective(aspect);
        self.update_proj_view();
    }
}

fn make_perspective(aspect: f32) -> na::Perspective3<f32> {
    // @hardcode fov, near, far
    let fov_rad = glm::pi::<f32>() / 3.0;
    let near = 0.1;
    let far = 100.0;
    na::Perspective3::new(aspect, fov_rad, near, far)
}

pub struct CameraSystem;

impl<'a> System<'a> for CameraSystem {
    type SystemData = (
        WriteExpect<'a, Camera>,
        Read<'a, Option<glutin::dpi::LogicalSize>>,
    );

    fn run(&mut self, (mut camera, resize): Self::SystemData) {
        if let Some(logical_size) = *resize {
            camera.resize((logical_size.width / logical_size.height) as f32);
        }
    }

    fn setup(&mut self, world: &mut World) {
        // @hardcode camera position, looking, and aspect ratio
        world.insert::<Camera>(Camera::new(
            [0.0, 0.0, 1.7],
            [0.0, 0.0, 0.0],
            2880.0 / 1620.0,
        ));
    }
}
