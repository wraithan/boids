use nalgebra_glm as glm;
use specs::{
    Component, ReadExpect, ReadStorage, System, VecStorage, World, Write, WriteExpect, WriteStorage,
};

use crate::physics::Physics;
use crate::quad_tree::{Circle, QuadTree};
use crate::render::{Renderable, Transform};
use crate::resources::WorldSettings;

#[derive(Default)]
pub struct ResetCommand(pub bool);

type TransformPhysicsTree = QuadTree<(Transform, Physics, BoidInfo)>;

#[derive(Copy, Clone, Debug, Component)]
#[storage(VecStorage)]
pub struct BoidInfo {
    pub flock_num: usize,
}

pub struct QuadTreeSystem;

impl<'a> System<'a> for QuadTreeSystem {
    type SystemData = (
        WriteExpect<'a, TransformPhysicsTree>,
        ReadExpect<'a, WorldSettings>,
        ReadStorage<'a, Transform>,
        ReadStorage<'a, Physics>,
        ReadStorage<'a, BoidInfo>,
    );

    fn run(&mut self, (mut quad_tree, settings, transform, physics, info): Self::SystemData) {
        use specs::Join;

        let min = settings.bounding_limit * -1.0;
        let max = settings.bounding_limit;

        // let timer = std::time::Instant::now();
        *quad_tree = QuadTree::new(5, min, max, 0, 16);
        // let mut count = 0;
        for (transform, physics, info) in (&transform, &physics, &info).join() {
            quad_tree.insert(transform.pos, (*transform, *physics, *info));
            // count += 1;
        }
        // println!("Quad build: {:?} ({})", timer.elapsed(), count);
    }

    fn setup(&mut self, world: &mut World) {
        world.insert::<TransformPhysicsTree>(QuadTree::new(1, glm::zero(), glm::zero(), 0, 8));
    }
}

pub struct FlockSystem;

type FlockSystemData<'a> = (
    Write<'a, ResetCommand>,
    ReadExpect<'a, WorldSettings>,
    ReadExpect<'a, TransformPhysicsTree>,
    ReadStorage<'a, Transform>,
    WriteStorage<'a, Physics>,
    WriteStorage<'a, Renderable>,
    ReadStorage<'a, BoidInfo>,
);

impl<'a> System<'a> for FlockSystem {
    type SystemData = FlockSystemData<'a>;

    fn run(
        &mut self,
        (mut reset_command, settings, quad_tree, transform, mut physics, mut renderable, info): Self::SystemData,
    ) {
        use rayon::prelude::*;
        use specs::{Join, ParJoin};

        // let timer = std::time::Instant::now();
        let reset = reset_command.0;
        if reset {
            use rand::Rng;

            *reset_command = ResetCommand(false);

            let mut rng = rand::thread_rng();
            for physics in (&mut physics).join() {
                physics.velocity.x = rng.gen_range(-physics.max_speed, physics.max_speed);
                physics.velocity.y = rng.gen_range(-physics.max_speed, physics.max_speed);
            }
        } else {
            (&transform, &mut physics, &mut renderable, &info)
                .par_join()
                .for_each(|(transform, physics, renderable, info)| {
                    let mut nearby_flock = vec![];
                    quad_tree.search(
                        &Circle {
                            center: transform.pos,
                            radius: settings.radius,
                        },
                        &mut nearby_flock,
                    );
                    let flock_size = nearby_flock.len().min(settings.max_influencers) as f32;
                    let mut count = 0.0;
                    if !nearby_flock.is_empty() {
                        let mut average_velocity: glm::Vec2 = glm::zero();
                        let mut average_position: glm::Vec2 = glm::zero();
                        for boid in nearby_flock
                            .into_iter()
                            .filter(|_boid| true)
                            .take(flock_size as usize)
                        {
                            if info.flock_num == boid.item.2.flock_num {
                                average_velocity += boid.item.1.velocity;
                                average_position += boid.item.0.pos;
                                count += 1.0;
                            }
                        }
                        average_velocity /= count;
                        average_position /= count;

                        let pos_dist = glm::distance(&transform.pos, &average_position);

                        let mut cohesion_force = average_position - transform.pos;
                        let mut separation_force = transform.pos - average_position;

                        average_velocity *= settings.velocity_weight;
                        cohesion_force *= settings.cohesion_weight;
                        separation_force *= ((settings.radius - pos_dist) / settings.radius)
                            * settings.separation_weight;

                        physics.add_force(average_velocity);
                        physics.add_force(cohesion_force);
                        physics.add_force(separation_force);
                    }

                    // let mag = glm::length(&physics.velocity);
                    // let speed_val = 1.0 - f32::min(mag / physics.max_speed, 1.0);
                    // By influence
                    let mut color_val = flock_size / (settings.max_influencers as f32);
                    // renderable.color[0] = 0.1;
                    // renderable.color[1] = 0.2;
                    // renderable.color[2] = speed_val;

                    // Purple
                    // let multiplier = 3.5;
                    // color_val = (color_val * multiplier).max(0.5).min(1.0);
                    // renderable.color[0] = color_val;
                    // renderable.color[1] = 1.0 - color_val;
                    // renderable.color[2] = color_val;

                    let multiplier = 3.5;
                    color_val = (color_val * multiplier).max(0.5).min(1.0);
                    renderable.color[0] = 1.0 - color_val;
                    renderable.color[1] = color_val;
                    renderable.color[2] = 1.0 / info.flock_num as f32;

                    // renderable.color[0] = 0.1;
                    // renderable.color[1] = 0.2;
                    // renderable.color[2] = 0.6;
                });
        }
        // println!("Flock processing: {:#?}", timer.elapsed());
    }
}
