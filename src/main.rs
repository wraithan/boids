mod camera;
mod flock;
mod physics;
mod quad_tree;
mod render;
mod resources;

use std::time::Duration;

use glutin::dpi::LogicalSize;
use glutin::WindowEvent;
use nalgebra_glm as glm;
use rand::Rng;
use specs::{Builder, World, WorldExt};

use crate::camera::CameraSystem;
use crate::flock::{BoidInfo, FlockSystem, QuadTreeSystem, ResetCommand};
use crate::physics::{Physics, PhysicsSystem};
use crate::render::{ColorFormat, DepthFormat, RenderSystem, Renderable, Transform};
use crate::resources::{DeltaTime, WorldSettings};

fn create_world(settings: WorldSettings) -> World {
    let mut world = World::new();
    world.register::<Transform>();
    world.register::<Physics>();
    world.register::<Renderable>();
    world.register::<BoidInfo>();
    world.insert(DeltaTime(Duration::from_millis(16)));
    // world.insert(DeltaTime(Duration::from_millis(7)));

    world.insert(settings);

    let mut rng = rand::thread_rng();
    let min = settings.bounding_limit * -1.0;
    let max = settings.bounding_limit;

    let scale = glm::vec2(0.05, 0.05);
    for i in 0..settings.num_boids {
        let speed = rng.gen_range(settings.min_speed, settings.max_speed);
        // let red = speed / max_speed;
        // let red = rng.gen_range(0.6, 1.0);
        let red = 0.0;
        let green = 0.5;
        // let blue = rng.gen_range(0.0, 0.1);
        let blue = speed / settings.max_speed;
        // let blue = 0.0;
        world
            .create_entity()
            .with(Transform {
                pos: glm::vec2(rng.gen_range(min.x, max.x), rng.gen_range(min.y, max.y)),
                scale,
            })
            .with(Physics {
                velocity: glm::vec2(rng.gen_range(-2.0, 2.0), rng.gen_range(-2.0, 2.0)),
                acceleration: glm::zero(),
                max_steering_force: rng.gen_range(0.01, 0.3),
                max_speed: speed,
                // max_steering_force: 0.1,
                // max_speed: 2.0,
            })
            .with(Renderable {
                color: [red, green, blue, 0.1].into(),
            })
            .with(BoidInfo { flock_num: i % 4 })
            .build();
    }

    world
}

fn main() {
    // init window
    let mut events_loop = winit::EventsLoop::new();

    let dpi_factor = events_loop.get_primary_monitor().get_hidpi_factor();

    // let raw_width = 2560.0 * 0.75;
    // let raw_height = 1440.0 * 0.75;
    let raw_width = 2560.0;
    let raw_height = 1440.0;
    // let raw_width = 3840.0; // * 0.75;
    // let raw_height = 2160.0; // * 0.75;
    let width = raw_width / dpi_factor;
    let height = raw_height / dpi_factor;
    let window_config = winit::WindowBuilder::new()
        .with_title("Project Boids")
        .with_dimensions((width, height).into());
    let context = glutin::ContextBuilder::new().with_vsync(true);
    let aspect_ratio = (width / height) as f32;
    let settings = WorldSettings::new(glm::vec2(aspect_ratio, 1.0)).expect("settings to load");

    let (windowed_context, device, factory, rtv, stv) =
        gfx_window_glutin::init::<ColorFormat, DepthFormat>(window_config, context, &events_loop)
            .expect("Failed to create window");

    if settings.fullscreen {
        let window = windowed_context.window();
        window.set_fullscreen(Some(window.get_current_monitor()));
    }

    let mut world = create_world(settings);
    world.insert::<Option<LogicalSize>>(None);
    let mut dispatcher = specs::DispatcherBuilder::new()
        .with(QuadTreeSystem, "quad_tree_system", &[])
        .with(FlockSystem, "flock_system", &["quad_tree_system"])
        .with(PhysicsSystem, "physics_system", &["flock_system"])
        .with(CameraSystem, "camera_system", &["physics_system"])
        .with_thread_local(RenderSystem::new(
            factory,
            windowed_context,
            device,
            rtv,
            stv,
        ))
        .build();

    dispatcher.setup(&mut world);

    let mut running = true;
    while running {
        events_loop.poll_events(|event| {
            if let glutin::Event::WindowEvent { event, .. } = event {
                match event {
                    WindowEvent::CloseRequested => running = false,
                    WindowEvent::Resized(logical_size) => {
                        world.insert(Some(logical_size));
                    }
                    WindowEvent::KeyboardInput {
                        input:
                            glutin::KeyboardInput {
                                virtual_keycode,
                                state,
                                ..
                            },
                        ..
                    } => {
                        if virtual_keycode == Some(glutin::VirtualKeyCode::Escape)
                            || virtual_keycode == Some(glutin::VirtualKeyCode::Q)
                        {
                            running = false;
                        }
                        if virtual_keycode == Some(glutin::VirtualKeyCode::R) {
                            world.insert(ResetCommand(true));
                            println!("Resetting velocity");
                        }
                        if virtual_keycode == Some(glutin::VirtualKeyCode::W)
                            && state == glutin::ElementState::Pressed
                        {
                            let mut settings = world.fetch_mut::<WorldSettings>();
                            settings.wrapping = !settings.wrapping;
                            println!("Wrapping: {}", settings.wrapping);
                        }
                    }

                    _ => (),
                }
            }
        });
        if !running {
            break;
        }

        dispatcher.dispatch(&world);
        world.maintain();
    }
}
