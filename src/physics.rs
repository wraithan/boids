use nalgebra_glm as glm;
use specs::{
    shred::ResourceId, Component, Read, ReadExpect, System, SystemData, VecStorage, World,
    WriteStorage,
};

use crate::render::Transform;
use crate::resources::{DeltaTime, WorldSettings};

#[derive(Copy, Clone, Debug, Component)]
#[storage(VecStorage)]
pub struct Physics {
    pub velocity: glm::Vec2,
    pub acceleration: glm::Vec2,
    pub max_steering_force: f32,
    pub max_speed: f32,
}

impl Physics {
    pub fn add_force(&mut self, force: glm::Vec2) {
        self.acceleration += force;
        // self.acceleration *= 0.0;
    }
}

pub struct PhysicsSystem;

#[derive(SystemData)]
pub struct PhysicsSystemData<'a> {
    delta: Read<'a, DeltaTime>,
    settings: ReadExpect<'a, WorldSettings>,
    transform: WriteStorage<'a, Transform>,
    physics: WriteStorage<'a, Physics>,
}

impl<'a> System<'a> for PhysicsSystem {
    type SystemData = PhysicsSystemData<'a>;

    fn run(
        &mut self,
        PhysicsSystemData {
            delta,
            settings,
            mut transform,
            mut physics,
        }: PhysicsSystemData,
    ) {
        use rayon::prelude::*;
        use specs::ParJoin;
        let delta = delta.0.subsec_millis() as f32 / 1000.0;
        let limit = settings.bounding_limit;

        (&mut transform, &mut physics)
            .par_join()
            .for_each(|(transform, physics)| {
                if !settings.wrapping {
                    let change = glm::vec2(transform.pos.x * -0.17, transform.pos.y * -0.1);
                    physics.add_force(change);
                }
                let acc_mag = glm::length(&physics.acceleration);
                if acc_mag > 0.0 {
                    if acc_mag > physics.max_steering_force {
                        physics.acceleration *= physics.max_steering_force / acc_mag;
                    }
                    physics.velocity += physics.acceleration;
                    let mag = glm::length(&physics.velocity);
                    if mag > physics.max_speed {
                        physics.velocity *= physics.max_speed / mag;
                    }

                    physics.acceleration *= 0.0;
                }
                transform.pos += physics.velocity * delta;

                if settings.wrapping {
                    if transform.pos.x > limit.x {
                        transform.pos.x -= limit.x * 2.0;
                    } else if transform.pos.x < -limit.x {
                        transform.pos.x += limit.x * 2.0;
                    }

                    if transform.pos.y > limit.y {
                        transform.pos.y -= limit.y * 2.0;
                    } else if transform.pos.y < -limit.y {
                        transform.pos.y += limit.y * 2.0;
                    }
                }

                debug_assert!(
                    !(transform.pos.x.is_nan() || transform.pos.y.is_nan()),
                    "pos has a NaN"
                );
                debug_assert!(
                    !(physics.velocity.x.is_nan() || physics.velocity.y.is_nan()),
                    "velocity has a NaN"
                );
            });
    }
}
