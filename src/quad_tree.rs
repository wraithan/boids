use nalgebra_glm as glm;

#[derive(Debug)]
pub struct QuadTree<T: Copy> {
    node: QuadTreeNode<T>,
    max_values: usize,
    values: Vec<PositionedItem<T>>,
    bounds: AABB,
    depth: usize,
    max_depth: usize,
}

#[derive(Copy, Clone, Debug)]
pub struct PositionedItem<T: Copy> {
    pub pos: glm::Vec2,
    pub item: T,
}

#[derive(Debug)]
pub struct AABB {
    pub mins: glm::Vec2,
    pub maxes: glm::Vec2,
}

#[derive(Debug)]
pub struct Circle {
    pub center: glm::Vec2,
    pub radius: f32,
}

#[derive(Debug)]
pub enum QuadTreeNode<T: Copy> {
    Children {
        neg_pos: Box<QuadTree<T>>,
        pos_pos: Box<QuadTree<T>>,
        pos_neg: Box<QuadTree<T>>,
        neg_neg: Box<QuadTree<T>>,
    },
    Terminal,
}

impl<T: Copy> QuadTree<T> {
    pub fn new(
        max_values: usize,
        mins: glm::Vec2,
        maxes: glm::Vec2,
        depth: usize,
        max_depth: usize,
    ) -> Self {
        Self {
            node: QuadTreeNode::Terminal,
            max_values,
            // @cleanup maybe preallocation should be a flag, needs profiling.
            values: Vec::with_capacity(max_values),
            bounds: AABB { mins, maxes },
            depth,
            max_depth,
        }
    }

    pub fn insert(&mut self, pos: glm::Vec2, item: T) -> bool {
        use QuadTreeNode::*;
        if self.contains(pos) {
            match self.node {
                Children {
                    ref mut neg_pos,
                    ref mut pos_pos,
                    ref mut pos_neg,
                    ref mut neg_neg,
                } => {
                    assert!(
                        neg_pos.insert(pos, item)
                            || pos_pos.insert(pos, item)
                            || pos_neg.insert(pos, item)
                            || neg_neg.insert(pos, item)
                    );
                }
                Terminal => {
                    if self.values.len() < self.max_values || self.depth >= self.max_depth {
                        self.values.push(PositionedItem { pos, item });
                    } else {
                        let half_width = (self.bounds.maxes.x - self.bounds.mins.x) / 2.0;
                        let half_height = (self.bounds.maxes.y - self.bounds.mins.y) / 2.0;
                        let depth = self.depth + 1;
                        let mut neg_pos = QuadTree::new(
                            self.max_values,
                            glm::vec2(self.bounds.mins.x, self.bounds.mins.y + half_height),
                            glm::vec2(self.bounds.maxes.x - half_width, self.bounds.maxes.y),
                            depth,
                            self.max_depth,
                        );
                        let mut pos_pos = QuadTree::new(
                            self.max_values,
                            glm::vec2(
                                self.bounds.mins.x + half_width,
                                self.bounds.mins.y + half_height,
                            ),
                            glm::vec2(self.bounds.maxes.x, self.bounds.maxes.y),
                            depth,
                            self.max_depth,
                        );
                        let mut pos_neg = QuadTree::new(
                            self.max_values,
                            glm::vec2(self.bounds.mins.x + half_width, self.bounds.mins.y),
                            glm::vec2(self.bounds.maxes.x, self.bounds.maxes.y - half_height),
                            depth,
                            self.max_depth,
                        );
                        let mut neg_neg = QuadTree::new(
                            self.max_values,
                            glm::vec2(self.bounds.mins.x, self.bounds.mins.y),
                            glm::vec2(
                                self.bounds.maxes.x - half_width,
                                self.bounds.maxes.y - half_height,
                            ),
                            depth,
                            self.max_depth,
                        );
                        for PositionedItem { pos, item } in self.values.drain(..) {
                            assert!(
                                neg_pos.insert(pos, item)
                                    || pos_pos.insert(pos, item)
                                    || pos_neg.insert(pos, item)
                                    || neg_neg.insert(pos, item)
                            );
                        }
                        self.node = Children {
                            neg_pos: Box::new(neg_pos),
                            pos_pos: Box::new(pos_pos),
                            pos_neg: Box::new(pos_neg),
                            neg_neg: Box::new(neg_neg),
                        }
                    }
                }
            };
            true
        } else {
            false
        }
    }

    fn contains(&self, pos: glm::Vec2) -> bool {
        pos.x >= self.bounds.mins.x
            && pos.x <= self.bounds.maxes.x
            && pos.y >= self.bounds.mins.y
            && pos.y <= self.bounds.maxes.y
    }

    pub fn search<'a>(&'a self, circle: &Circle, results: &mut Vec<&'a PositionedItem<T>>) {
        use QuadTreeNode::*;

        if circle_intersects_aabb(&circle, &self.bounds) {
            match self.node {
                Children {
                    ref neg_pos,
                    ref pos_pos,
                    ref pos_neg,
                    ref neg_neg,
                } => {
                    neg_pos.search(circle, results);
                    pos_pos.search(circle, results);
                    pos_neg.search(circle, results);
                    neg_neg.search(circle, results);
                }
                Terminal => {
                    for item in &self.values {
                        if circle_intersects_point(circle, item.pos) {
                            results.push(item);
                        }
                    }
                }
            }
        }
    }
}

fn circle_intersects_aabb(circle: &Circle, aabb: &AABB) -> bool {
    let mut closest = circle.center;
    if circle.center.x < aabb.mins.x {
        closest.x = aabb.mins.x;
    } else if circle.center.x > aabb.maxes.x {
        closest.x = aabb.maxes.x;
    }
    if circle.center.y < aabb.mins.y {
        closest.y = aabb.mins.y;
    } else if circle.center.y > aabb.maxes.y {
        closest.y = aabb.maxes.y;
    }

    glm::distance2(&circle.center, &closest) < (circle.radius * circle.radius)
}

#[test]
fn circle_intersect_aabb_tests() {
    // Not intersecting
    let circle = Circle {
        center: glm::vec2(0.5, 0.5),
        radius: 0.5,
    };
    let aabb = AABB {
        mins: glm::vec2(-1.5, -1.5),
        maxes: glm::vec2(-0.5, -0.5),
    };
    assert!(!circle_intersects_aabb(&circle, &aabb));

    // Easily intersecting
    let circle = Circle {
        center: glm::vec2(0.5, 0.5),
        radius: 0.5,
    };
    let aabb = AABB {
        mins: glm::vec2(-0.5, -0.5),
        maxes: glm::vec2(0.25, 0.25),
    };
    assert!(circle_intersects_aabb(&circle, &aabb));
}

fn circle_intersects_point(circle: &Circle, point: glm::Vec2) -> bool {
    glm::distance2(&circle.center, &point) < (circle.radius * circle.radius)
}

#[test]
fn circle_intersects_point_tests() {
    // Not intersecting
    let circle = Circle {
        center: glm::vec2(0.5, 0.5),
        radius: 0.5,
    };
    let point = glm::vec2(-1.5, -1.5);
    assert!(!circle_intersects_point(&circle, point));

    // Easily intersecting
    let circle = Circle {
        center: glm::vec2(0.5, 0.5),
        radius: 0.5,
    };
    let point = glm::vec2(0.25, 0.25);
    assert!(circle_intersects_point(&circle, point));
}
