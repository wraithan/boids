use gfx::traits::FactoryExt;
use gfx::{
    self, gfx_constant_struct_meta, gfx_defines, gfx_impl_struct_meta, gfx_pipeline,
    gfx_pipeline_inner, gfx_vertex_struct_meta, Device,
};
use gfx_device_gl as device_gl;
use glutin::dpi::LogicalSize;
use nalgebra_glm as glm;
use specs::{Component, LazyUpdate, Read, ReadExpect, ReadStorage, RunNow, VecStorage, World};

use crate::camera::Camera;

pub type ColorFormat = gfx::format::Srgba8;
pub type DepthFormat = gfx::format::DepthStencil;

gfx_defines! {
    #[derive(Default)]
    vertex Vertex {
        pos: [f32; 2] = "in_pos",
        uv: [f32; 2] = "in_uv",
    }

    #[derive(Default)]
    constant Uniforms {
        color: [f32; 4] = "Uniforms.color",
        proj_view: [[f32; 4]; 4] = "Uniforms.proj_view",
        boid_pos: [f32; 2] = "Uniforms.boid_pos",
        boid_scale: [f32; 2] = "Uniforms.boid_scale",
        // time_ms: f32 = "Uniforms.time_ms",
        // material: i32 = "Uniforms.material_type",
    }

    pipeline pipe {
        vbuf: gfx::VertexBuffer<Vertex> = (),
        uniforms: gfx::ConstantBuffer<Uniforms> = "Uniforms",
        out_color: gfx::BlendTarget<ColorFormat> = (
            "Target0",
            gfx::state::ColorMask::all(),
            gfx::preset::blend::ALPHA
        ),
        out_depth: gfx::DepthTarget<DepthFormat> = gfx::preset::depth::LESS_EQUAL_TEST,
    }
}

const VERTEX_SHADER: &[u8] = include_bytes!("../resources/shaders/base.glslv");
const FRAGMENT_SHADER: &[u8] = include_bytes!("../resources/shaders/base.glslf");

#[derive(Copy, Clone, Debug, Component)]
#[storage(VecStorage)]
pub struct Transform {
    pub pos: glm::Vec2,
    pub scale: glm::Vec2,
}

#[derive(Debug, Component)]
#[storage(VecStorage)]
pub struct Renderable {
    pub color: glm::Vec4,
}

pub struct RenderSystem {
    // factory: device_gl::Factory,
    encoder: gfx::Encoder<device_gl::Resources, device_gl::CommandBuffer>,
    windowed_context: glutin::WindowedContext<glutin::PossiblyCurrent>,
    device: device_gl::Device,

    quad_slice: gfx::Slice<device_gl::Resources>,
    pso: gfx::PipelineState<device_gl::Resources, pipe::Meta>,
    data: pipe::Data<device_gl::Resources>,
}

type RenderSystemData<'a> = (
    Read<'a, Option<LogicalSize>>,
    ReadExpect<'a, Camera>,
    ReadStorage<'a, Transform>,
    ReadStorage<'a, Renderable>,
    Read<'a, LazyUpdate>,
);

impl RenderSystem {
    pub fn new(
        mut factory: device_gl::Factory,
        windowed_context: glutin::WindowedContext<glutin::PossiblyCurrent>,
        device: device_gl::Device,
        rtv: gfx::handle::RenderTargetView<device_gl::Resources, ColorFormat>,
        stv: gfx::handle::DepthStencilView<device_gl::Resources, DepthFormat>,
    ) -> Self {
        let encoder = gfx::Encoder::from(factory.create_command_buffer());

        let vertices = [
            Vertex {
                pos: [-1.0, -1.0],
                uv: [0.0, 0.0],
            },
            Vertex {
                pos: [1.0, -1.0],
                uv: [1.0, 0.0],
            },
            Vertex {
                pos: [-1.0, 1.0],
                uv: [0.0, 1.0],
            },
            Vertex {
                pos: [1.0, 1.0],
                uv: [1.0, 1.0],
            },
        ];

        #[rustfmt::skip]
        let indices = [
            0u16, 1, 2,
               2, 1, 3,
        ];

        let (vbuf, quad_slice) = factory.create_vertex_buffer_with_slice(&vertices, &indices[..]);

        let uniforms = factory.create_constant_buffer(1);

        let data = pipe::Data {
            vbuf,
            uniforms,
            out_color: rtv,
            out_depth: stv,
        };

        let shader_set = factory
            .create_shader_set(VERTEX_SHADER, FRAGMENT_SHADER)
            .unwrap();

        let rasterizer = gfx::state::Rasterizer {
            // method: gfx::state::RasterMethod::Line(1),
            ..gfx::state::Rasterizer::new_fill()
        };

        let pso = factory
            .create_pipeline_state(
                &shader_set,
                gfx::Primitive::TriangleList,
                rasterizer,
                pipe::new(),
            )
            .expect("Couldn't build debug triangle depth pipeline");

        Self {
            // factory,
            encoder,
            windowed_context,
            device,
            quad_slice,
            pso,
            data,
        }
    }
}

impl<'a> RunNow<'a> for RenderSystem {
    fn run_now(&mut self, world: &'a World) {
        use specs::Join;

        let (resize, camera, transform, renderable, updater): RenderSystemData =
            world.system_data();

        if let Some(logical_size) = *resize {
            let dpi_factor = self.windowed_context.window().get_hidpi_factor();
            let physical_size = logical_size.to_physical(dpi_factor);
            self.windowed_context.resize(physical_size);

            let (new_color, new_depth) =
                gfx_window_glutin::new_views::<ColorFormat, DepthFormat>(&self.windowed_context);

            self.data.out_color = new_color;
            self.data.out_depth = new_depth;
            updater.exec_mut(|world| world.insert::<Option<LogicalSize>>(None));
        }

        // Clear out render buffers
        self.encoder
            .clear(&self.data.out_color, [0.0, 0.0, 0.0, 1.0]);
        self.encoder.clear_depth(&self.data.out_depth, 1.0);

        // Iterate renderables and encode commands
        for (transform, renderable) in (&transform, &renderable).join() {
            let uniforms = Uniforms {
                boid_pos: transform.pos.into(),
                boid_scale: transform.scale.into(),
                color: renderable.color.into(),
                proj_view: camera.proj_view.into(),
            };

            self.encoder
                .update_buffer(&self.data.uniforms, &[uniforms], 0)
                .expect("failed to update data.uniforms buffer");

            self.encoder.draw(&self.quad_slice, &self.pso, &self.data);
        }

        // Send commands
        self.encoder.flush(&mut self.device);
        self.windowed_context
            .swap_buffers()
            .expect("Couldn't swap buffers?");
        self.device.cleanup();
    }

    fn setup(&mut self, _world: &mut World) {
        self.encoder
            .clear(&self.data.out_color, [0.0, 0.0, 0.0, 1.0]);
        self.encoder.clear_depth(&self.data.out_depth, 1.0);
    }
}
