use std::fs;
use std::io;
use std::time::Duration;

use nalgebra_glm as glm;
use serde::Deserialize;

#[derive(Default)]
pub struct DeltaTime(pub Duration);

#[derive(Copy, Clone, Debug, Deserialize)]
#[serde(default)]
pub struct WorldSettings {
    #[serde(skip)]
    pub bounding_limit: glm::Vec2,
    pub wrapping: bool,
    pub min_speed: f32,
    pub max_speed: f32,
    pub radius: f32,
    pub velocity_weight: f32,
    pub separation_weight: f32,
    pub cohesion_weight: f32,
    pub max_influencers: usize,
    pub num_boids: usize,
    pub fullscreen: bool,
}

impl Default for WorldSettings {
    fn default() -> Self {
        Self {
            bounding_limit: glm::vec2(1.0, 1.0),
            wrapping: false,
            min_speed: 0.0,
            max_speed: 1.0,
            radius: 0.1,
            velocity_weight: 1.0,
            separation_weight: 0.8,
            cohesion_weight: 0.6,
            max_influencers: 100,
            num_boids: 1000,
            fullscreen: true,
        }
    }
}

impl WorldSettings {
    pub fn new(bounding_limit: glm::Vec2) -> io::Result<Self> {
        println!(
            "Loading {}{}settings.toml",
            std::env::current_dir()?.display(),
            std::path::MAIN_SEPARATOR
        );
        let raw_settings = fs::read_to_string("settings.toml")?;

        let mut settings: WorldSettings = toml::from_str(&raw_settings)?;
        settings.bounding_limit = bounding_limit;
        println!("Settings loaded: {:#?}", settings);
        Ok(settings)
    }
}
